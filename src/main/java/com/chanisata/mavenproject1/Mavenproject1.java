/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.chanisata.mavenproject1;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Mavenproject1 {
        private static char[][] table;
        private static char player;
        
        public Mavenproject1() {
                System.out.println("Welcome to OX Game");
                table = new char[3][3];
                player = 'X';

                for (int row = 0; row < 3; row++) {
                       for (int cols = 0; cols < 3; cols++) {
                              table[row][cols] = ' ';
                              }
                       }
            }

        public static void Table() {
            System.out.println("------------------");
            for (int rows = 0; rows < 3; rows++) {
                for (int cols = 0; cols < 3; cols++) {
                    System.out.print("| " + table[rows][cols] + " | ");
                }
                System.out.println("\n------------------");
            }
        }

        public boolean Full() {
            for (int rows = 0; rows < 3; rows++) {
                for (int cols = 0; cols < 3; cols++) {
                    if (table[rows][cols] == ' ') {
                        return false;
                    }
                }
            }
            return true;
        }

    private boolean checkWinner() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == player && table[i][1] == player && table[i][2] == player) {
                return true;
            }
        }

        for (int j = 0; j < 3; j++) {
            if (table[0][j] == player && table[1][j] == player && table[2][j] == player) {
                return true;
            }
        }

        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            return true;
        }
        if (table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            return true;
        }
        return false;

    }

        public void playGame() {
            Scanner sc = new Scanner(System.in);

            while (true) {
                Table();
                System.out.println();
                System.out.println("> " + player + " Turn <");
                System.out.print(player + " Please enter the required rows and columns :");

                String input = sc.nextLine();

                if (input.matches("\\d\\d")) {

                    int row = Character.getNumericValue(input.charAt(0) - 1);
                    int col = Character.getNumericValue(input.charAt(1) - 1);

                    if (row >= 0 && row < 3 && col >= 0 && col < 3 && table[row][col] == ' ') {
                    table[row][col] = player;

                        if (checkWinner()) {
                            Table();
                            System.out.println(player + " be a winner!");
                            break;
                        }

                        if (Full()) {
                            Table();
                            System.out.println("Both players are tied.");
                            break;
                        }

                        player = (player == 'X') ? 'O' : 'X';
                    } 
                }
            }
        }

        public static void main(String[] args) {
            Mavenproject1 OX = new Mavenproject1();
            OX.playGame();

        }
}
